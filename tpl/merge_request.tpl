{user_name} has made a merge request for {target_project_name}:

  {merge_request_title}
  {merge_request_url}
