#!/usr/bin/env python

import requests
import argparse
import json


def _parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--uri', default='http://localhost:8081/merge-request',
        help='the uri to which send the example request')
    parser.add_argument(
        '--tls-verify', action='store_true',
        help='whether to verify the SSL/TLS certificate')
    return parser.parse_args()


# merge-request example taken from https://gitlab.com/help/web_hooks/web_hooks
def _send_request(uri):
    headers = {
        "X-Gitlab-Event": "Merge Request Hook",
    }

    body = {
        "object_kind": "merge_request",
        "user": {
            "name": "Administrator",
            "username": "root",
            "avatar_url": "http://www.gravatar.com/avatar/"
                          "e64c7d89f26bd1972efa854d13d7dd61?s=40"
                          "\u0026d=identicon"
        },
        "object_attributes": {
            "id": 99,
            "target_branch": "master",
            "source_branch": "ms-viewport",
            "source_project_id": 14,
            "author_id": 51,
            "assignee_id": 6,
            "title": "MS-Viewport",
            "created_at": "2013-12-03T17: 23: 34Z",
            "updated_at": "2013-12-03T17: 23: 34Z",
            "st_commits": None,
            "st_diffs": None,
            "milestone_id": None,
            "state": "opened",
            "merge_status": "unchecked",
            "target_project_id": 14,
            "iid": 1,
            "description": "",
            "source": {
                "name": "Awesome Project",
                "description": "Aut reprehenderit ut est.",
                "web_url": "http://example.com/awesome_space/awesome_project",
                "avatar_url": None,
                "git_ssh_url": "git@example.com:awesome_space/"
                               "awesome_project.git",
                "git_http_url": "http://example.com/awesome_space/"
                                "awesome_project.git",
                "namespace": "Awesome Space",
                "visibility_level": 20,
                "path_with_namespace": "awesome_space/awesome_project",
                "default_branch": "master",
                "homepage": "http://example.com/awesome_space/awesome_project",
                "url": "http://example.com/awesome_space/awesome_project.git",
                "ssh_url": "git@example.com:awesome_space/awesome_project.git",
                "http_url": "http://example.com/awesome_space/"
                            "awesome_project.git"
            },
            "target": {
                "name": "Awesome Project",
                "description": "Aut reprehenderit ut est.",
                "web_url": "http://example.com/awesome_space/awesome_project",
                "avatar_url": None,
                "git_ssh_url": "git@example.com:awesome_space/"
                               "awesome_project.git",
                "git_http_url": "http://example.com/awesome_space/"
                                "awesome_project.git",
                "namespace": "Awesome Space",
                "visibility_level": 20,
                "path_with_namespace": "awesome_space/awesome_project",
                "default_branch": "master",
                "homepage": "http://example.com/awesome_space/awesome_project",
                "url": "http://example.com/awesome_space/awesome_project.git",
                "ssh_url": "git@example.com:awesome_space/awesome_project.git",
                "http_url": "http://example.com/awesome_space/"
                            "awesome_project.git"
            },
            "last_commit": {
                "id": "da1560886d4f094c3e6c9ef40349f7d38b5d27d7",
                "message": "fixed readme",
                "timestamp": "2012-01-03T23: 36: 29+02: 00",
                "url": "http://example.com/awesome_space/awesome_project/"
                       "commits/"
                       "da1560886d4f094c3e6c9ef40349f7d38b5d27d7",
                "author": {
                    "name": "GitLab dev user",
                    "email": "gitlabdev@dv6700.(none)"
                }
            },
            "work_in_progress": False,
            "url": "http://example.com/diaspora/merge_requests/1",
            "action": "open",
            "assignee": {
                "name": "User1",
                "username": "user1",
                "avatar_url": "http://www.gravatar.com/avatar/"
                              "e64c7d89f26bd1972efa854d13d7dd61?"
                              "s=40\u0026d=identicon"
            }
        }
    }
    requests.post(
        uri, headers=headers, data=json.dumps(body),
        verify=(args.tls_verify))


def _main(args):
    _send_request(args.uri)


if __name__ == '__main__':
    args = _parse_args()
    _main(args)
