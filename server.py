#!/usr/bin/env python
# server.py
# Copyright (C) 2016 LEAP
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


"""
A simple HTTP server to format requests from GitLab web hooks and send them
through email.
"""


import os
import argparse
import ConfigParser
import json
import sys

from klein import Klein
from twisted.python import log
from twisted.internet import reactor
from twisted.internet import ssl
from twisted.web.server import Site

from util import sendmail


FROM_ADDRESS = None
TO_ADDRESS = None
BASEDIR = os.path.dirname(os.path.realpath(__file__))
TEMPLATEDIR = os.path.join(BASEDIR, 'tpl')

MERGE_REQUEST_TEMPLATE = os.path.join(TEMPLATEDIR, 'merge_request.tpl')


def _extract_data(body):
    attrs = body['object_attributes']
    data = {
        'user_name': body['user']['name'],
        'target_project_name': attrs['target']['name'],
        'merge_request_title': attrs['title'],
        'merge_request_url': attrs['url'],
    }
    return data


def _send_merge_request_email(body):
    subject = "New merge request from Gitlab!"
    data = _extract_data(body)
    with open(MERGE_REQUEST_TEMPLATE) as f:
        body = f.read().format(**data)
    sendmail(FROM_ADDRESS, TO_ADDRESS, subject, body)


def _verify_token(request, token):
    allow = False
    if token is None:
        allow = True
    elif request.getHeader('X-Gitlab-Token') == token:
        allow = True
    return allow


def _parse_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        '--host', default='0.0.0.0',
        help='the hostname or IP address to bind the listening socket to')
    parser.add_argument(
        '--port', default=8081, type=int, help='the port in which to run')
    parser.add_argument(
        '--to-address', default='foo@bar',
        help='the email address to which messages will be sent')
    parser.add_argument(
        '--from-address', default='bar@foo',
        help='the email address from which to send messages')
    parser.add_argument(
        '--token', help='only accept requests if token is correct')
    parser.add_argument(
        '--x509-cert-file',
        help='a file containing a x509 certificate and key')
    parser.add_argument(
        '--config-file',
        help='read configuration from file (override command line options)')
    return parser.parse_args()


def _read_config_from_file(args):
    config = ConfigParser.ConfigParser()
    config.read(args.config_file)
    options = [
        'host', 'port', 'to-address', 'from-address', 'token',
        'x509-cert-file'
    ]
    for option in options:
        try:
            value = config.get('server', option, None)
            if option == 'port':
                value = int(value)
            setattr(args, option.replace('-', '_'), value)
        except ConfigParser.NoOptionError:
            pass


class KleinSSL(Klein):

    def runSSL(self, host, port, certFile, logFile=None):
        if logFile is None:
            logFile = sys.stdout
        log.startLogging(logFile)
        # get SSL cert
        with open(certFile) as f:
            certificate = ssl.PrivateCertificate.loadPEM(f.read())
        # run server
        reactor.listenSSL(
            port,
            Site(self.resource()),
            certificate.options(),
            interface=host)
        reactor.run()


def _main(args):
    app = None
    params = None
    meth = None
    if args.x509_cert_file:
        app = KleinSSL()
        params = [args.host, args.port, args.x509_cert_file]
        meth = app.runSSL
    else:
        app = Klein()
        params = args.host, args.port
        meth = app.run

    @app.route('/merge-request')
    def merge_request(request):
        if _verify_token(request, args.token):
            body = json.loads(request.content.read())
            reactor.callLater(0, _send_merge_request_email, body)
            return 'OK'
        # unauthorized!
        request.setResponseCode(401)
        return 'Unauthorized.'

    meth(*params)


if __name__ == "__main__":
    # get options from command line and file
    args = _parse_args()
    if args.config_file and os.path.isfile(args.config_file):
        _read_config_from_file(args)
    FROM_ADDRESS = args.from_address
    TO_ADDRESS = args.to_address
    # run server
    _main(args)
