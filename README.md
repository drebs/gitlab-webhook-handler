GitLab web hook emailer
=======================

A simple HTTP server to format requests from GitLab web hooks and send them
through email.

Use the `--help` option to see available command line options.

You can use a configuration file to set options, and they will override
command line options. See the file `default.conf.example` for an example.

Check the GitLab Webhook documentation for more information:

  https://gitlab.com/help/web_hooks/web_hooks

Example workflow
----------------

The following diagram is an example of how this handler is intended to work
for handling a merge request webhook:

    o
   /|\    1. dev pushes a feature branch
   / \      .------------------------.
developer-->|     developer repo     |---------------.
    |       '------------------------'               |
    |                                                v
    |                                   .------------------------.
    '---------------------------------->|     gitlab server      |
       2. dev creates merge request     '------------------------'
                                                     |
                                        3. gitlab triggers a webhook
                                                     |
                                                     v
                                        .------------------------.
                                        | gitlab-webhook-handler |
                                        '------------------------'
                                                     |
                                4. webhook handler sends an email to an
                                   address notifying about the event
                                                     |
                                                     v
                                                  ______
                                                 |      |\
                                                 |new   '-|
                                                 |merge   |
                                                 |request!|
                                                 |________|
